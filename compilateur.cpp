//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, CHAR, DOUBLE};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string,TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id)
{
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s)
{
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Type := INTEGER | BOOLEAN | CHAR | DOUBLE
// Identifier := alpha{alpha|digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
// DisplayStatement := "DISPLAY" Expression

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Identifier {"," Identifier} ":" Type 

// CaseLabel ::= Number | CharConst
// CaseLabelList ::= CaseLabel {, CaseLabel }
// CaseListElement ::= CaseLabelList : Statement | Empty
// CaseStatement ::= CASE Expression OF CaseListElement{; CaseListElement} END

// Identifier := alpha{alpha|digit}
TYPES Identifier(void)
{
	TYPES identifierType = DeclaredVariables[lexer->YYText()];

	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();

	return identifierType;
}


// Number := Digit{Digit}
TYPES Number(void){
	bool is_a_decimal=false;
	double d;					// 64-bit float
	unsigned int *i;			// pointer to a 32 bit unsigned int 
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){
		// Floating point constant number
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; // i points to the const double
		//cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to : 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{ // Integer Constant
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	
}

// Char := ' '
TYPES Char(void)
{
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

//Type := INTEGER | BOOLEAN | CHAR | DOUBLE
TYPES Type(void)
{
	TYPES type;

	if(current != TYPE)
	{
		Error("type était attendu");
	}
	if(strcmp(lexer->YYText(),"INTEGER")==0)
		type=INTEGER;
	else if(strcmp(lexer->YYText(),"BOOLEAN")==0)
		type=BOOLEAN;
	else if(strcmp(lexer->YYText(),"CHAR")==0)
		type=CHAR;
	else if(strcmp(lexer->YYText(),"DOUBLE")==0)
		type=DOUBLE;
	
	current=(TOKEN) lexer->yylex();

	return type;
}

TYPES Expression(void);			// Called by Term() and calls Term()

// Factor := Number | identifier | "(" Expression ")"| True/False | char 
TYPES Factor(void)
{
	TYPES factorType;

	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		factorType = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else if (current==NUMBER)
	{
		factorType = Number();
	}
	else if(current==ID)
	{
		factorType = Identifier();
	}
	else if(current == KEYWORD)
	{
		factorType = BOOLEAN;
		if(strcmp(lexer->YYText(),"True")==0)
		{
			cout <<"\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		}
		else if(strcmp(lexer->YYText(),"False")==0)
		{
			cout <<"\tpush $0x0\t\t# False"<<endl;	
		}
		current=(TOKEN) lexer->yylex();
	}
	else if(current==CHARCONST)
	{
		factorType = Char();
	}
		else
			Error("'(' , chiffre, boolean ou identificateur attendu");

	return factorType;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void)
{
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void)
{
	TYPES termType;
	OPMUL mulop;

	termType = Factor();

	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		if(termType != Factor())
		{
			Error("Operation sur deux types differents");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq %rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq %rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}

	return termType;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void)
{
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void)
{
	TYPES simpleExpressionType;
	OPADD adop;

	simpleExpressionType = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if(simpleExpressionType != Term())
		{
			Error("Operation sur deux types differents");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}

	return simpleExpressionType;
}

void VarDeclaration();
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "." 
void VarDeclarationPart()
{
	if(strcmp(lexer->YYText(),"VAR")!=0)
	{
		Error("VAR était attendu");
	}

	VarDeclaration();

	while(current == SEMICOLON) 
	{
		VarDeclaration();
	}

	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}
// VarDeclaration := Identifier {"," Identifier} ":" Type 
void VarDeclaration()
{
	vector<string> addedVariables;

	do
	{
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");

		// Check if the identifier is already used
		if(IsDeclared(lexer->YYText()))
			Error("Erreur : Variable '"+ (string)lexer->YYText() + "' déjà déclarée");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;

		DeclaredVariables.insert(pair<string,TYPES>(lexer->YYText(),INTEGER));
		addedVariables.push_back(lexer->YYText());

		current=(TOKEN) lexer->yylex();
	}
	while(current == COMMA);

	if(current != DOUBLEDOT)
	{
		Error(" : était attendu");
	}
	current=(TOKEN) lexer->yylex();

	TYPES varType = Type();

	for(int i=0; i<addedVariables.size(); i++)
	{
		DeclaredVariables[addedVariables[i]] = varType;
	}
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void)
{
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPES Expression(void)
{
	TYPES expressionType;
	OPREL oprel;

	expressionType = SimpleExpression();

	if(current==RELOP){
		oprel=RelationalOperator();

		if(expressionType != SimpleExpression())
		{
			Error("Comparaison entre deux type differents");
		}

		if(expressionType!=DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjge Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjle Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjl Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tjg Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;

		expressionType = BOOLEAN;
	}

	return expressionType;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void)
{
	string variable;

	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	TYPES identifierType = INTEGER;
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if(identifierType != Expression())
	{
		Error("variable et expression n'ont pas le même type");
	}
	cout << "\tpop "<<variable<<endl;
	return variable;
}

void Statement(void);

//IfStatement := "IF" Boolean "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void)
{
	unsigned long long tag = ++TagNumber;

	if(strcmp(lexer->YYText(),"IF")!=0)
	{
		Error("IF était attendu");
	}
	cout << "\t\t#Debut IF" << tag << endl;

	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOLEAN)
	{
		Error("Expression booleenne attendu");
	}

	cout << "\tpop %rax" <<  endl;
	cout << "\tcmpq $0xFFFFFFFFFFFFFFFF , %rax" << endl;
	cout << "\tjne else" << tag << endl;
	
	// THEN
	if(strcmp(lexer->YYText(),"THEN")!=0)
	{
		Error("THEN était attendu");
	}

	current=(TOKEN) lexer->yylex();
	cout << "\t\t#THEN" << tag << endl;
	Statement();
	cout << "\tjmp next" << tag << endl;

	// ELSE
	cout << "\t\t#ELSE" << tag << endl;
	cout << "else" << tag << ":" << endl;
	if(strcmp(lexer->YYText(),"ELSE")==0)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout << "\t\t#fin IF" << tag << endl;
	cout << "next" << tag << ":" << endl;
}

//WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void)
{
	unsigned long long tag = ++TagNumber;
	if(strcmp(lexer->YYText(),"WHILE")!=0)
	{
		Error("WHILE était attendu");
	}
	cout << "\t\t#Debut WHILE" << tag << endl;
	cout << "startwhile" << tag << ":" << endl;

	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOLEAN)
	{
		Error("Expression booleenne attendu");
	}

	cout << "\tpop %rax" <<  endl;
	cout << "\tcmpq $0xFFFFFFFFFFFFFFFF , %rax" << endl;
	cout << "\tjne next" << tag << endl;
	
	// DO 
	if(strcmp(lexer->YYText(),"DO")!=0)
	{
		Error("DO était attendu");
	}
	
	current=(TOKEN) lexer->yylex();
	cout << "\t\t#DO" << tag << endl;
	Statement();
	cout << "jmp startwhile" << tag << endl;
	cout << "\t\t#fin WHILE" << tag << endl;
	cout << "next" << tag << ":" << endl;
}

//ForStatement := "FOR" AssignementStatement "TO"/"DOWNTO" Expression "DO" Statement
void ForStatement(void)
{
	unsigned long long tag = ++TagNumber;
	if(strcmp(lexer->YYText(),"FOR")!=0)
	{
		Error("FOR était attendu");
	}
	cout << "\t\t#Debut FOR" << tag << endl;

	current=(TOKEN) lexer->yylex();
	string variable = AssignementStatement();

	bool up;
	// To 
	if(strcmp(lexer->YYText(),"TO")==0)
	{
		up = true;
	}
	else if(strcmp(lexer->YYText(),"DOWNTO")==0)
	{
		up = false;
	}
	else
	{
		Error("TO/DOWNTO était attendu");
	}
	
	current=(TOKEN) lexer->yylex();
	cout << "\t\t#To" << tag << endl;

	cout << "startfor" << tag << ":" << endl;
	Expression();
	cout << "\tpop %rax" <<  endl;

	// DO 
	if(strcmp(lexer->YYText(),"DO")!=0)
	{
		Error("DO était attendu");
	}
	

	cout << "\tcmpq %rax," << variable << endl;
	cout << "\tje next" << tag << endl;
	cout << "\t\t#DO" << tag << endl;

	current=(TOKEN) lexer->yylex();
	Statement();

	if(up)
		cout << "\taddq $1, " << variable << "\t#add 1 to " << variable << endl;
	else
		cout << "\tsubq $1, " << variable << "\t#sub 1 from " << variable << endl;
	cout << "jmp startfor" << tag << endl;
	cout << "\t\t#fin FOR" << tag << endl;
	cout << "next" << tag << ":" << endl;
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
	if(strcmp(lexer->YYText(),"BEGIN")!=0)
	{
		Error("BEGIN était attendu");
	}
	cout << "\t\t#Debut BLOCK" << endl;

	current=(TOKEN) lexer->yylex();
	Statement();

	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(strcmp(lexer->YYText(),"END")!=0)
	{
		Error("END était attendu");
	}
	current=(TOKEN) lexer->yylex();
	cout << "\t\t#FIN BLOCK" << endl;


}

//DisplayStatement := "DISPLAY" Expression
void DisplayStatement(){
	unsigned long long tag = ++TagNumber;

	if(strcmp(lexer->YYText(),"DISPLAY")!=0)
	{
		Error("DISPLAY était attendu");
	}
	cout << "\t\t#Debut DISPLAY" << endl;

	current=(TOKEN) lexer->yylex();

	switch(Expression()){
		case INTEGER:
			cout << "\tpop %rsi\t# The value to be displayed"<<endl;
			cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		case BOOLEAN:
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
			break;
		case DOUBLE:
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
			break;
		case CHAR:
			cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		default:
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
	}

	cout << "\t\t#fin DISPLAY" << endl;
}


//CaseLabel ::= Number | CharConst
void CaseLabel(TYPES expressionType, int nbCase, int caseTag)
{
	TYPES caseLabelType;

	if (current==NUMBER)
	{
		caseLabelType = Number();
	}
	else if(current==CHARCONST)
	{
		caseLabelType = Char();
	}
	else
	{
		Error("une valeur constante était attendu");
	}

	if(expressionType != caseLabelType)
		Error("Comparaison impossible entre deux type different");

	cout << "\tpop %rbx" << endl;
	cout << "\tcmpq %rax, %rbx" << endl;
	cout << "\tje caseStatement" << caseTag << "_" << nbCase << endl;
}

//CaseLabelList ::= CaseLabel {, CaseLabel }
void CaseLabelList(TYPES expressionType, int nbCase, int caseTag)
{
	CaseLabel(expressionType, nbCase, caseTag);
	while(current == COMMA)
	{
		current=(TOKEN) lexer->yylex();
		CaseLabel(expressionType, nbCase, caseTag);
	}
}

//CaseListElement ::= CaseLabelList : Statement | Empty
void CaseListElement(TYPES expressionType, int nbCase, int caseTag)
{
	cout << "case" << caseTag << "_" << nbCase << ":" << endl;

	CaseLabelList(expressionType, nbCase, caseTag);

	cout << "\tjmp case" << caseTag << "_" << nbCase+1 << endl;

	if(current != DOUBLEDOT)
	{
		Error(" : était attendu");
	}

	cout << "caseStatement" << caseTag << "_" << nbCase << ":" << endl;

	current=(TOKEN) lexer->yylex();
	if(strcmp(lexer->YYText(),"END")!=0 && current != SEMICOLON) // if not empty
		Statement();

	cout << "\tjmp endCase" << caseTag << endl;
}

//CaseStatement ::= CASE Expression OF CaseListElement{; CaseListElement} END
void CaseStatement()
{
	unsigned long long tag = ++TagNumber;
	unsigned long long nbCase = 0;

	if(strcmp(lexer->YYText(),"CASE")!=0)
	{
		Error("CASE était attendu");
	}
	cout << "\t\t#Debut CASE" << endl;

	current=(TOKEN) lexer->yylex();

	TYPES expressionType = Expression();
	cout << "\tpop %rax" <<  endl;

	if(strcmp(lexer->YYText(),"OF")!=0)
	{
		Error("OF était attendu");
	}

	current=(TOKEN) lexer->yylex();

	cout << "\t\t#CASE" << nbCase << endl;
	CaseListElement(expressionType,nbCase,tag);
	nbCase++;
	
	while(current == SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		cout << "\t\t#CASE" << nbCase << endl;
		CaseListElement(expressionType, nbCase, tag);
		nbCase++;
	}
	cout << "case" << tag << "_" << nbCase << ":" << endl;
	cout << "endCase" << tag << ":" << endl;
	cout << "\t\t#fin CASE" << endl;

	if(strcmp(lexer->YYText(),"END")!=0)
	{
		Error("END était attendu");
	}

	current=(TOKEN) lexer->yylex();
}
//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement
void Statement(void)
{
	if(current == KEYWORD)
	{
		if(strcmp(lexer->YYText(),"IF")==0)
		{
			IfStatement();
		}
		else if(strcmp(lexer->YYText(),"WHILE")==0)
		{
			WhileStatement();
		}
		else if(strcmp(lexer->YYText(),"FOR")==0)
		{
			ForStatement();
		}
		else if(strcmp(lexer->YYText(),"BEGIN")==0)
		{
			BlockStatement();
		}
		else if(strcmp(lexer->YYText(),"DISPLAY")==0)
		{
			DisplayStatement();
		}
		else if(strcmp(lexer->YYText(),"CASE")==0)
		{
			CaseStatement();
		}
	}
	else if(current == ID)
	{
		AssignementStatement();
	}
	else
		Error("mot inconnu");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void)
{
	cout << "\t.align 8"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void)
{
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();	
}

int main(void)
{	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\""<<endl;
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl;
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl;
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl;
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl;

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}

//chaine - entier signé - procedure/fonction 
